//
//  JavaRandom.h
//  PasswordCardApp
//
//  Created by Jeff Clement on 10-12-16.
//  Copyright 2010 Jeff Clement. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface JavaRandom : NSObject {
@private long long randomSeed;
}

@property long long randomSeed;

-(id) initWithSeed:(unsigned long long) seed1;
-(int) next:(int)bits;
//-(int) nextInt;
-(unsigned long long) nextUnsignedLongLong;
-(int) nextInt: (int) n;

@end
