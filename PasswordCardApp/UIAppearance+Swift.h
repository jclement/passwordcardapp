//
//  UIAppearance+Swift.h
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-23.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

#ifndef UIAppearance_Swift_h
#define UIAppearance_Swift_h

#import <UIKit/UIKit.h>

// UIAppearance+Swift.h
@interface UIView (UIViewAppearance_Swift)
// appearanceWhenContainedIn: is not available in Swift. This fixes that.
+ (instancetype)my_appearanceWhenContainedIn:(Class<UIAppearanceContainer>)containerClass;
@end

#endif /* UIAppearance_Swift_h */
