# PasswordCard App for iOS

This is the iOS companion application to Pepsoft's [Passwordcard.org](http://www.passwordcard.org/).

It is compatible and card numbers generated from the website will render an identical card.