//
//  UIAppearance+Swift.m
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-23.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIAppearance+Swift.h"

// UIAppearance+Swift.m
@implementation UIView (UIViewAppearance_Swift)
+ (instancetype)my_appearanceWhenContainedIn:(Class<UIAppearanceContainer>)containerClass {
    return [self appearanceWhenContainedIn:containerClass, nil];
}
@end