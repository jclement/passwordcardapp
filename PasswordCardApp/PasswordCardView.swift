//
//  PasswordCardView.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-21.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import Foundation
import UIKit

class PasswordCardView : UIView {
    let CARD_ROWS = 9
    let CARD_COLS = 29
    
    let HEADER_CHARS : NSString = "■□▲△○●★☂☀☁☹☺♠♣♥♦♫€¥£$!?¡¿⊙◐◩�"
    let DIGITS: NSString = "0123456789"
    let DIGITS_AND_LETTERS : NSString = "23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"
    let DIGITS_LETTERS_AND_SYMBOLS : NSString = "23456789abcdefghjkmnpqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ@#$%&*<>?€+{}[]()/\\";
    
    private let topOffset : CGFloat
    
    init(frame: CGRect, offset: CGFloat) {
        topOffset = offset
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        topOffset = 20
        super.init(coder: aDecoder)
    }
    
    private func generateCard() -> [NSString] {
        
        // convert hex card number into unsigned long long
        let scanner = NSScanner(string: AppSettings.cardNumber!)
        var cardNumber : CUnsignedLongLong = 0
        scanner.scanHexLongLong(&cardNumber)
        print("Redrawing")
       
        // seed RNG with card number
        //let seed = CLongLong(cardNumber)
        let random = JavaRandom(seed: cardNumber)
        
        var rows = [NSString]()
        
        // shuffle header row and trim to card width
        let headerRow = NSMutableString(format: "%@", HEADER_CHARS);
        shuffle(headerRow, withJavaRandom: random)
        headerRow.deleteCharactersInRange(NSMakeRange(CARD_COLS, headerRow.length - CARD_COLS))
        rows.append(headerRow)
       
        let halfHeight = 1 + ((CARD_ROWS - 1) / 2);
        for(var y=1; y<CARD_ROWS; y += 1)
        {
            let currentRow = NSMutableString()
            for (var x=0; x<CARD_COLS; x += 1)
            {
                // select characterset based on settings + position
                let charSet : NSString
                if (y >= halfHeight && AppSettings.includeDigits) {
                    charSet = DIGITS
                } else {
                    if (AppSettings.includeSymbols && (x%2)==0) {
                        charSet = DIGITS_LETTERS_AND_SYMBOLS
                    } else {
                        charSet = DIGITS_AND_LETTERS
                    }
                }
                // find random next character and add to the card
                let nextIndex = Int(random.nextInt(Int32(charSet.length)))
                let nextChar = charSet.substringWithRange(NSMakeRange(nextIndex, 1))
                currentRow.appendString(nextChar)
            }
            rows.append(currentRow)
        }
        return rows
    }
    
    // shuffle the characters in a string according to RNG
    private func shuffle(str: NSMutableString, withJavaRandom random: JavaRandom) {
        for(var i=str.length; i > 1; i -= 1)
        {
            swap(str, withX: i-1, withY: Int(random.nextInt(Int32(i))))
        }
    }
    
    // swap to characters in a string
    private func swap(str: NSMutableString, withX x: Int, withY y: Int) {
        let tmp = str.substringWithRange(NSMakeRange(x, 1))
        str.replaceCharactersInRange(NSMakeRange(x, 1), withString: str.substringWithRange(NSMakeRange(y, 1)))
        str.replaceCharactersInRange(NSMakeRange(y, 1), withString: tmp)
    }
   
    // force redraw because card options changed
    func update() {
        self.setNeedsDisplay()
    }
    
    override func drawRect(rect: CGRect) {
        let card = generateCard()
        
        let context = UIGraphicsGetCurrentContext()
        let colorSpaceRef = CGColorSpaceCreateDeviceRGB()
        //let topOffset : CGFloat = 20
        let charOffset : CGFloat = 15
        let rowHeight = (self.bounds.size.height - topOffset) / CGFloat(CARD_ROWS)
        let charWidth = (self.bounds.size.width - charOffset) / CGFloat(CARD_COLS)
        let fontSize :CGFloat = 22
        
        let colors: [[CGFloat]] = [
            [1,1,1,1],
            [1,1,1,1],
            [0.8,0.8,0.8,1],
            [1,0.8,0.8,1],
            [0.8,1,0.8,1],
            [1,1,0.8,1],
            [0.8,0.8,1,1],
            [1,0.8,1,1],
            [0.8,1,1,1]
        ]
        
        // build colored rows
        for(var i=0; i<CARD_ROWS; i+=1) {
            CGContextSetFillColorWithColor(context, CGColorCreate(colorSpaceRef, colors[i]))
            CGContextFillRect(context, CGRectMake(0, topOffset + CGFloat(i) * rowHeight, self.bounds.size.width, topOffset + rowHeight + 10))
        }
        
        let blackColor : [CGFloat] = [0,0,0,1]
        CGContextSetFillColorWithColor(context, CGColorCreate(colorSpaceRef,blackColor));
        
        // fill in the rows
        for(var row=0; row<CARD_ROWS; row += 1)
        {
            if (row>=1)
            {
                let rowHeader = NSString(format: "%d", row)
                let point = CGPointMake(4, topOffset + CGFloat(row)*rowHeight+2)
                let font = UIFont(name:"Helvetica", size:10)!
                rowHeader.drawAtPoint(point, withAttributes: [
                    NSFontAttributeName: font
                    ])
            }
            let currentRow : NSString = card[row]
            for (var col=0; col<CARD_COLS; col+=1)
            {
                let currentChar : NSString = currentRow.substringWithRange(NSRange(location: col, length: 1))
                let point = CGPointMake(charOffset+CGFloat(col)*charWidth, topOffset + 10+CGFloat(row)*rowHeight)
                let font = UIFont(name:"FreeMono", size:fontSize)!
                currentChar.drawAtPoint(point, withAttributes: [
                    NSFontAttributeName: font
                    ])
            }
        }

    }
}
