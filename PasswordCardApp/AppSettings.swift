//
//  AppSettings.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-21.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import Foundation
import SSKeychain

class AppSettings {
    
    private static let kCardNumber = "CARD_NUMBER"
    private static let kFirstRun = "FIRST_RUN"
    private static let kIncludeDigits = "INCLUDE_DIGITS"
    private static let kIncludeSymbols = "INCLUDE_SYMBOLS"
    
    private static let settings : NSUserDefaults = {
        let settings = NSUserDefaults.standardUserDefaults()
        settings.registerDefaults([
            kFirstRun: true,
            kIncludeDigits: false,
            kIncludeSymbols: false
            ])
        return settings
    }()
    
    // Card Number is stored in the Keychain
    
    static func generateRandomCardNumber() -> String {
        // TBD
        var arr = [UInt8](count: 8, repeatedValue:0)
        
        let fd = open("/dev/random", O_RDONLY)
        if fd != -1 {
            read(fd, &arr, sizeofValue(arr[0]) * arr.count)
            close(fd)
        }
        
        let newCardNumber : NSMutableString = NSMutableString(string: "")
        for el in arr {
            newCardNumber.appendFormat("%02x", el)
        }
        return NSString(string: newCardNumber) as String
    }
    
    static var cardNumber: String? {
        get {
            return SSKeychain.passwordForService(kCardNumber, account: "user")
        }
        set {
            SSKeychain.setPassword(newValue, forService: kCardNumber, account: "user")
        }
    }
   
    // and the normal settings
    
    static var firstRun: Bool {
        get {
            return settings.boolForKey(kFirstRun)
        }
        set {
            settings.setBool(newValue, forKey: kFirstRun)
        }
    }
    
    static var includeDigits: Bool {
        get {
            return settings.boolForKey(kIncludeDigits)
        }
        set {
            settings.setBool(newValue, forKey: kIncludeDigits)
        }
    }
    
    static var includeSymbols: Bool {
        get {
            return settings.boolForKey(kIncludeSymbols)
        }
        set {
            settings.setBool(newValue, forKey: kIncludeSymbols)
        }
    }
}
