//
//  AppDelegate.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-21.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import UIKit
import ChameleonFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        
        MyChameleon.setGlobalThemeUsingPrimaryColor(UIColor.flatBlueColorDark(), withContentStyle: UIContentStyle.Light)
        
        if AppSettings.firstRun {
            // attempt to migrate old password card across
            migrateOldSettings()
            AppSettings.firstRun = false
        }
        
        if AppSettings.cardNumber == nil {
            // generate a new password card if we don't have one
            AppSettings.cardNumber = AppSettings.generateRandomCardNumber()
        }
      
        return true
    }
    
    private func migrateOldSettings() {
        
        let oldCardNumber = NSUserDefaults.standardUserDefaults().stringForKey("cardNumber")
        let oldIncludeDigits = NSUserDefaults.standardUserDefaults().boolForKey("includeDigits")
        let oldIncludeSymbols = NSUserDefaults.standardUserDefaults().boolForKey("includeSymbols")
        
        if let oldCardNumber = oldCardNumber {
            AppSettings.cardNumber = oldCardNumber
            AppSettings.includeDigits = oldIncludeDigits
            AppSettings.includeSymbols = oldIncludeSymbols
            print("Migrated old card")
        }
        
        NSUserDefaults.standardUserDefaults().removeObjectForKey("cardNumber")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("includeDigits")
        NSUserDefaults.standardUserDefaults().removeObjectForKey("includeSymbols")
        
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

