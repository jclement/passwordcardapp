//
//  HelpWebViewController.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-22.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import Foundation
import UIKit

class HelpViewController : UIViewController, UIWebViewDelegate {
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let path = NSBundle.mainBundle().pathForResource("index", ofType: "html", inDirectory: "about")!
        
        let url = NSURL(string: path)!
        
        let request = NSURLRequest(URL: url)
        
        webView.loadRequest(request)
        
        webView.delegate = self
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        
        if request.URL!.scheme == "file" {
            return true
        } else {
            UIApplication.sharedApplication().openURL(request.URL!)
            return false
        }
    }
}