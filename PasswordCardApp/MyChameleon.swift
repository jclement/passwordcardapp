//
//  MyChameleon.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-23.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import Foundation
import ChameleonFramework

class MyChameleon: Chameleon {
    static func customizeButtonWithPrimaryColor(primaryColor: UIColor, withContentStyle contentStyle: UIContentStyle) {
        UIButton.appearance().tintColor = primaryColor
        
        UIButton.my_appearanceWhenContainedIn(UINavigationBar.self).tintColor = UIColor.whiteColor()
        UIButton.my_appearanceWhenContainedIn(UINavigationBar.self).backgroundColor = UIColor.clearColor()
        
        UIButton.my_appearanceWhenContainedIn(UIToolbar.self).tintColor = UIColor.whiteColor()
        UIButton.my_appearanceWhenContainedIn(UIToolbar.self).backgroundColor = UIColor.clearColor()
    }
}
