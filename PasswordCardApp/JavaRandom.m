//
//  JavaRandom.m
//  PasswordCardApp
//
//  Created by Jeff Clement on 10-12-16.
//  Copyright 2010 Jeff Clement. All rights reserved.
//

#import "JavaRandom.h"


@implementation JavaRandom

static long long multiplier = 0x5DEECE66DL;
static long addend = 0xBL;
static long long mask = 281474976710655; //(0x1000000000000001L << 48) - 1; -- This was throwing a warning

-(id) init
{
    self = [super init];
    if (self != nil)
    {
		@synchronized(self)
		{
			randomSeed = (arc4random() ^ multiplier) & mask;
		}
    }
    return self;
}

-(id) initWithSeed:(unsigned long long) seed1 {
	self = [super init];
    if (self != nil)
    {
        @synchronized(self)
		{
			randomSeed = (seed1 ^ multiplier) & mask;
		}
    }
    return self;
}

-(int) next:(int)bits {
    int ret;
    @synchronized(self)
    {
        long long oldseed, nextseed;
        long long seed1 = randomSeed;
        //do {
		oldseed = seed1;
		nextseed = (oldseed * multiplier + addend) & mask;
        //} while (!seed.compareAndSet(oldseed, nextseed)); /* you won't need to do this atomically */
        randomSeed = nextseed;
        ///ret = (int)(nextseed >>> (48 - bits));
        ret = (unsigned int)(nextseed >> (48 - bits));
    }
	
    return ret;
	
}

-(int) nextInt
{
	return [self next:32];
}

-(int) nextInt: (int) n
{
	if (n < 0)
		@throw [NSException exceptionWithName:@"IllegalArgumentException" reason:@"n must be positive" userInfo:nil];
	if ((n & -n) == n) // i.e. n is a power of 2
	{
		long long r = [self next:31];
		return (int)((n *r)>>31);
	}
	int bits;
	int val;
	do {
		bits = [self next: 31];
		val = bits % n;
	} while ((bits - val + (n-1)) < 0);
	return val;
}

-(unsigned long long) nextUnsignedLongLong
{
	return (((unsigned long long)[self nextInt])<<32) + [self nextInt];
}

-(long long) randomSeed
{
	return self->randomSeed;
}

-(void) setRandomSeed: (long long) randomSeedArg
{
	self->randomSeed = randomSeedArg;
}

@end
