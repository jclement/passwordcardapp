//
//  ConfigurationViewController.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-22.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import Foundation
import UIKit
import PKHUD
import QRCodeReader
import AVFoundation

class ConfigurationViewController : UITableViewController, UITextFieldDelegate, QRCodeReaderViewControllerDelegate {
    
    lazy var reader: QRCodeReaderViewController = QRCodeReaderViewController(cancelButtonTitle: "Cancel", coderReader: QRCodeReader(metadataObjectTypes: [AVMetadataObjectTypeQRCode]), showTorchButton: true)
    
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var includeDigitsSwitch: UISwitch!
    @IBOutlet weak var includeSymbolsSwitch: UISwitch!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        cardNumberTextField.text = AppSettings.cardNumber
        includeDigitsSwitch.setOn(AppSettings.includeDigits, animated: false)
            includeSymbolsSwitch.setOn(AppSettings.includeSymbols, animated: false)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ConfigurationViewController.hideKeyboard(_:)))
        gestureRecognizer.cancelsTouchesInView = false
        tableView.addGestureRecognizer(gestureRecognizer)
    }
    
    
    @IBAction func cardNumberDone(sender: AnyObject) {
    }
    
    @IBAction func done(sender: AnyObject) {
        cardNumberTextField.resignFirstResponder()
        
        let anythingModified = !(
            AppSettings.cardNumber == cardNumberTextField.text?.lowercaseString &&
            AppSettings.includeDigits == includeDigitsSwitch.on &&
            AppSettings.includeSymbols == includeSymbolsSwitch.on
        )
        
        if (anythingModified) {
            AppSettings.cardNumber = cardNumberTextField.text?.lowercaseString
            AppSettings.includeDigits = includeDigitsSwitch.on
            AppSettings.includeSymbols = includeSymbolsSwitch.on

            PKHUD.sharedHUD.contentView = PKHUDSuccessView()
            PKHUD.sharedHUD.show()
            PKHUD.sharedHUD.hide(afterDelay: 0.5);
        }
        
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func cancel(sender: AnyObject) {
        cardNumberTextField.resignFirstResponder()
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func generateNewCard(sender: AnyObject) {
        cardNumberTextField.resignFirstResponder()
        cardNumberTextField.text = AppSettings.generateRandomCardNumber()
        validate()
    }
    
    @IBAction func scanCard(sender: AnyObject) {
        
        guard QRCodeReader.supportsMetadataObjectTypes() else {
            let alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: "OK", style: .Cancel, handler: nil))
            presentViewController(alert, animated: true, completion: nil)
            return
        }
        
        cardNumberTextField.resignFirstResponder()
        reader.modalPresentationStyle = .FormSheet
        reader.delegate               = self
        
        reader.completionBlock = { (result: String?) in
            if let result = result {
                let components = (result as NSString).componentsSeparatedByString(",") as NSArray
                let cardNumber = components.objectAtIndex(0) as! String
                
                let regex: NSRegularExpression
                do {
                    regex = try NSRegularExpression(pattern: "^[abcdef0123456789]{1,16}$", options: NSRegularExpressionOptions.CaseInsensitive)
                } catch {
                    return
                }
            
                let validNumber = regex.matchesInString(cardNumber, options: NSMatchingOptions(), range: NSMakeRange(0, (cardNumber as NSString).length)).count > 0
    
                if validNumber {
                    PKHUD.sharedHUD.contentView = PKHUDSuccessView()
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay: 0.5);
                    
                    self.cardNumberTextField.text = cardNumber
                    self.includeDigitsSwitch.setOn(components.containsObject("d"), animated: false)
                    self.includeSymbolsSwitch.setOn(components.containsObject("s"), animated: false)
                    self.validate()
                } else {
                    PKHUD.sharedHUD.contentView = PKHUDErrorView()
                    PKHUD.sharedHUD.show()
                    PKHUD.sharedHUD.hide(afterDelay: 0.5);
                }
            }
        }
        presentViewController(reader, animated: true, completion: nil)
    }
    
    private func validate() {
        validate(cardNumberTextField.text!)
    }
    
    private func validate(cardNumber: String) {
        let regex: NSRegularExpression
        do {
            regex = try NSRegularExpression(pattern: "^[abcdef0123456789]{1,16}$", options: NSRegularExpressionOptions.CaseInsensitive)
        } catch {
            return
        }
        
        let valid = regex.matchesInString(cardNumber, options: NSMatchingOptions(), range: NSMakeRange(0, (cardNumber as NSString).length)).count > 0
        
        
        doneButton.enabled = valid
        cardNumberTextField.backgroundColor = valid ? UIColor.whiteColor() : UIColor.flatRedColor()
        cardNumberTextField.textColor = valid ? UIColor.blackColor() : UIColor.whiteColor()
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        let oldText : NSString = textField.text!
        let newText : NSString = oldText.stringByReplacingCharactersInRange(range, withString: string)
        validate(newText as String)
        return true
    }
    
    func hideKeyboard(gestureRecognizer: UIGestureRecognizer) {
        cardNumberTextField.resignFirstResponder()
    }
    
    // MARK: - QR Code
    
    func reader(reader: QRCodeReaderViewController, didScanResult result: String) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func readerDidCancel(reader: QRCodeReaderViewController) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
