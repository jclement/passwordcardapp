//
//  PasswordCardViewController.swift
//  PasswordCardApp
//
//  Created by Jeff Clement on 2015-11-22.
//  Copyright © 2015 Jeff Clement. All rights reserved.
//

import Foundation
import UIKit

class PasswordCardViewController : UIViewController {
    
    @IBOutlet weak var passwordCardView: PasswordCardView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        passwordCardView.update()
    }
    
    @IBAction func shareCard(sender: AnyObject) {
        let view = PasswordCardView(frame: CGRect.init(x: 0, y: 0, width: 568, height: 308-20), offset: 0)
        view.update()
        
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, true, 0)
        view.drawViewHierarchyInRect(view.bounds, afterScreenUpdates: true)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        let controller = UIActivityViewController.init(activityItems: [image], applicationActivities: nil)
        self.presentViewController(controller, animated: true, completion: nil)
        
    }

}